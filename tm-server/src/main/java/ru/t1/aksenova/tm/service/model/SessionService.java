package ru.t1.aksenova.tm.service.model;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.api.repository.model.ISessionRepository;
import ru.t1.aksenova.tm.api.service.IConnectionService;
import ru.t1.aksenova.tm.api.service.model.ISessionService;
import ru.t1.aksenova.tm.enumerated.Role;
import ru.t1.aksenova.tm.exception.entity.SessionNotFoundException;
import ru.t1.aksenova.tm.exception.field.DateEmptyException;
import ru.t1.aksenova.tm.exception.field.IdEmptyException;
import ru.t1.aksenova.tm.exception.field.UserIdEmptyException;
import ru.t1.aksenova.tm.exception.user.RoleEmptyException;
import ru.t1.aksenova.tm.model.Session;
import ru.t1.aksenova.tm.model.User;
import ru.t1.aksenova.tm.repository.model.SessionRepository;

import javax.persistence.EntityManager;
import java.util.Date;
import java.util.List;

public class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    public SessionService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    public ISessionRepository getRepository(@NotNull final EntityManager entityManager) {
        return new SessionRepository(entityManager);
    }

    @NotNull
    @Override
    public Session create(
            @Nullable final String userId,
            @Nullable final String role
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (role == null || role.isEmpty()) throw new RoleEmptyException();
        @NotNull Session session = new Session();
        session.setRole(Role.valueOf(role));
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        session.setUser(entityManager.find(User.class, userId));
        add(session);
        entityManager.close();
        return session;
    }

    @NotNull
    @Override
    public List<Session> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            return repository.findAll(userId);
        } finally {
            entityManager.close();
        }
    }


    @Nullable
    @Override
    public Session findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            @Nullable final Session session = repository.findOneById(userId, id);
            if (session == null) throw new SessionNotFoundException();
            return session;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clear() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.clear(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }


    @Nullable
    @Override
    public Session removeOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Session session = findOneById(userId, id);
        remove(userId, session);
        return session;
    }


    @NotNull
    @Override
    public Session updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String role,
            @Nullable final Date date
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (role == null || role.isEmpty()) throw new RoleEmptyException();
        if (date == null) throw new DateEmptyException();
        @Nullable final Session session = findOneById(userId, id);
        if (session == null) throw new SessionNotFoundException();
        session.setRole(Role.valueOf(role));
        session.setDate(date);
        update(userId, session);
        return session;
    }


    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepository repository = getRepository(entityManager);
            return (repository.findOneById(userId, id) != null);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public long getSize(@Nullable final String userId) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionRepository repository = getRepository(entityManager);
            return repository.getCount(userId);
        } finally {
            entityManager.close();
        }
    }

}
