package ru.t1.aksenova.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.api.repository.dto.IUserDTORepository;
import ru.t1.aksenova.tm.dto.model.UserDTO;
import ru.t1.aksenova.tm.enumerated.Role;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

public interface IUserDTOService extends IDTOService<UserDTO> {

    @NotNull
    IUserDTORepository getRepository(@NotNull EntityManager entityManager);

    @NotNull
    UserDTO create(
            @Nullable String login,
            @Nullable String password
    );

    @NotNull
    UserDTO create(
            @Nullable String login,
            @Nullable String password,
            @Nullable String email
    );

    @NotNull
    UserDTO create(
            @Nullable String login,
            @Nullable String password,
            @Nullable Role role
    );

    @NotNull Collection<UserDTO> set(@NotNull Collection<UserDTO> users);

    @NotNull
    List<UserDTO> findAll();

    @Nullable
    UserDTO findOneById(@Nullable String id);

    @NotNull
    UserDTO findByLogin(@Nullable String login);

    @NotNull
    UserDTO findByEmail(@Nullable String email);

    @Nullable
    UserDTO removeOneById(@Nullable String id);

    void clear();

    @Nullable
    UserDTO removeOne(@Nullable UserDTO user);

    @Nullable
    UserDTO removeOneByLogin(@Nullable String login);

    @Nullable
    UserDTO removeOneByEmail(@Nullable String email);

    @NotNull
    UserDTO setPassword(
            @Nullable String id,
            @Nullable String password
    );

    @NotNull
    UserDTO updateUser(
            @Nullable String id,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName
    );

    boolean isLoginExist(@Nullable String login);

    boolean isEmailExist(@Nullable String email);

    void lockUserByLogin(@Nullable String login);

    void unlockUserByLogin(@Nullable String login);

}
