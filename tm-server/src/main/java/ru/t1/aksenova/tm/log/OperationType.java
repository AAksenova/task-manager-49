package ru.t1.aksenova.tm.log;

public enum OperationType {

    INSERT,
    UPDATE,
    DELETE;

}
