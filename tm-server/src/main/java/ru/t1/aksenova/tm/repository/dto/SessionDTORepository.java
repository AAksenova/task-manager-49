package ru.t1.aksenova.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.api.repository.dto.ISessionDTORepository;
import ru.t1.aksenova.tm.dto.model.SessionDTO;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class SessionDTORepository extends AbstractUserOwnedDTORepository<SessionDTO> implements ISessionDTORepository {

    public SessionDTORepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public List<SessionDTO> findAll() {
        @NotNull final String jpql = "SELECT m FROM SessionDTO m";
        return entityManager.createQuery(jpql, SessionDTO.class).getResultList();
    }

    @NotNull
    @Override
    public List<SessionDTO> findAll(@NotNull final String userId) {
        if (userId.isEmpty() || userId == null) return Collections.emptyList();
        @NotNull final String jpql = "SELECT m FROM SessionDTO m WHERE m.userId = :userId";
        return entityManager.createQuery(jpql, SessionDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public SessionDTO findOneById(@NotNull final String id) {
        if (id.isEmpty() || id == null) return null;
        return entityManager.find(SessionDTO.class, id);
    }

    @Nullable
    @Override
    public SessionDTO findOneById(@NotNull final String userId, @NotNull final String id) {
        if (id.isEmpty() || id == null) return null;
        if (userId.isEmpty() || userId == null) return null;
        @NotNull final String jpql = "SELECT m FROM SessionDTO m WHERE m.userId = :userId and m.id = :id";
        return entityManager.createQuery(jpql, SessionDTO.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void removeById(@NotNull final String id) {
        Optional<SessionDTO> model = Optional.ofNullable(findOneById(id));
        model.ifPresent(this::remove);
    }

    @Override
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        Optional<SessionDTO> model = Optional.ofNullable(findOneById(id, userId));
        model.ifPresent(this::remove);
    }

    @Override
    public void clear() {
        @NotNull final String jpql = "DELETE FROM SessionDTO";
        entityManager.createQuery(jpql).executeUpdate();
    }

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final String jpql = "DELETE FROM SessionDTO m WHERE m.userId = :userId";
        entityManager.createQuery(jpql)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public long getCount() {
        @NotNull final String jpql = "SELECT COUNT(m) FROM SessionDTO m";
        return entityManager.createQuery(jpql, Long.class).getSingleResult();
    }

    @Override
    public long getCount(@NotNull final String userId) {
        @NotNull final String jpql = "SELECT COUNT(m) FROM SessionDTO m WHERE m.userId = :userId";
        return entityManager.createQuery(jpql, Long.class)
                .setParameter("userId", userId)
                .getSingleResult();
    }

    @Override
    public boolean existById(@NotNull final String id) {
        @NotNull final String jpql = "SELECT COUNT(m) = 1 FROM SessionDTO m WHERE m.id = :id";
        return entityManager.createQuery(jpql, Boolean.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Override
    public boolean existById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String jpql = "SELECT COUNT(m) = 1 FROM SessionDTO m WHERE m.id = :id AND m.userId = :userId";
        return entityManager.createQuery(jpql, Boolean.class)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .getSingleResult();
    }
}
