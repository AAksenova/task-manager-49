package ru.t1.aksenova.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.aksenova.tm.model.AbstractUserOwnedModel;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    M add(@NotNull String userId, M model);

    void update(@NotNull String userId, M model);

    void remove(@NotNull String userId, M model);

}
