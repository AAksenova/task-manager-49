package ru.t1.aksenova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.api.endpoint.IAuthEndpoint;
import ru.t1.aksenova.tm.api.endpoint.IUserEndpoint;
import ru.t1.aksenova.tm.command.AbstractCommand;
import ru.t1.aksenova.tm.dto.model.UserDTO;


public abstract class AbstractUserCommand extends AbstractCommand {

    @NotNull
    protected IUserEndpoint getUserEndpointClient() {
        return getServiceLocator().getUserEndpointClient();
    }

    @NotNull
    protected IAuthEndpoint getAuthEndpointClient() {
        return getServiceLocator().getAuthEndpointClient();
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    protected void showUser(@Nullable final UserDTO user) {
        if (user == null) return;
        System.out.println("USER ID: " + user.getId());
        System.out.println("USER LOGIN: " + user.getLogin());
        System.out.println("USER E-MAIL: " + user.getEmail());
        System.out.println("USER ROLE: " + user.getRole().getDisplayName());
    }

}
