package ru.t1.aksenova.tm.api.service;


import org.jetbrains.annotations.NotNull;
import ru.t1.aksenova.tm.api.endpoint.*;

public interface IServiceLocator {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ISystemEndpoint getSystemEndpointClient();

    @NotNull
    IProjectEndpoint getProjectEndpointClient();

    @NotNull
    ITaskEndpoint getTaskEndpointClient();

    @NotNull
    IAuthEndpoint getAuthEndpointClient();

    @NotNull
    IUserEndpoint getUserEndpointClient();

    @NotNull
    ITokenService getTokenService();

    IAdminEndpoint getAdminEndpointClient();

}
